let validator = {
  kiemTraRong: (value, errorId, message) => {
    if (value.length == 0) {
      document.getElementById(
        errorId
      ).innerText = `${message} không được để trống`;
      return false;
    } else {
      document.getElementById(errorId).innerText = "";
      return true;
    }
  },
  kiemTraSo: function (value, errorId, message) {
    if (Number.isInteger(value)) {
      document.getElementById(errorId).innerText = "";
      return true;
    } else {
      document.getElementById(errorId).innerText = message;
      return false;
    }
  },
  checkInputValid: (phone) => {
    let isNameValid = validator.kiemTraRong(
      phone.name,
      "spanTenSP",
      "Tên sản phẩm"
    );
    let isType = validator.kiemTraRong(
      phone.type,
      "spanHangSP",
      "Hãng sản xuất"
    );
    let isPriceValid =
      validator.kiemTraRong(phone.price, "spanGiaSP", "Giá sản phẩm") &&
      validator.kiemTraSo(phone.price * 1, "spanGiaSP", "Giá phải là một số");
    let isImageValid = validator.kiemTraRong(
      phone.img,
      "spanHinhSP",
      "Hình sản phẩm"
    );
    let isScreenValid = validator.kiemTraRong(
      phone.screen,
      "spanManHinhSP",
      "Thông tin"
    );
    let isFontCameraValid = validator.kiemTraRong(
      phone.frontCamera,
      "spanCameraSauSP",
      "Thông tin"
    );
    let isBackCameraValid = validator.kiemTraRong(
      phone.backCamera,
      "spanCameraTruocSP",
      "Thông tin"
    );
    let isDescValid = validator.kiemTraRong(phone.desc, "spanMoTa", "Mô tả");
    let isValid =
      isNameValid &
      isType &
      isPriceValid &
      isImageValid &
      isScreenValid &
      isFontCameraValid &
      isBackCameraValid &
      isDescValid;
    return isValid;
  },
};
