const BASE_URL = "https://62db6ca2e56f6d82a77284cd.mockapi.io/products-data";

let productList = [];
let edittingItemId;

function getData() {
  onGetDataSpinner();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      offGetDataSpinner();
      productList = res.data;
      renderTable(productList);
    })
    .catch((err) => {
      offGetDataSpinner();
      console.log(err);
    });
}
getData();

function addProducts() {
  let newPhone = getUserInput();
  let isValid = validator.checkInputValid(newPhone);
  if (isValid) {
    onLoadingBtn("add-product-btn");
    axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: newPhone,
    })
      .then((res) => {
        getData();
        offLoadingBtn("add-product-btn", "Add Product");
        hideModal();
        clearInputModal();
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

function delelteItem(id) {
  onLoadingBtn(`delete-btn-${id}`);
  axios({ url: `${BASE_URL}/${id}`, method: "DELETE" })
    .then((res) => {
      getData();
    })
    .catch((err) => {
      offLoadingBtn(
        `delete-btn-${id}`,
        `<i class="fa-solid fa-trash-can"></i>`
      );
      alert("Fail to delete, check connection!");
      console.log(err);
    });
}

function editItem(id) {
  showModal();
  showModalForEdit();
  showEditInfo(id);
  edittingItemId = id;
}

function updateItem() {
  let newItem = getUserInput();
  console.log("newItem: ", newItem);
  let isValid = validator.checkInputValid(newItem);
  if (isValid) {
    onLoadingBtn("edit-product-btn");
    axios({
      url: `${BASE_URL}/${edittingItemId}`,
      method: "PUT",
      data: newItem,
    })
      .then((res) => {
        offLoadingBtn("edit-product-btn", "Update Product");
        getData();
        hideModal();
      })
      .catch((err) => {
        offLoadingBtn("edit-product-btn", "Update Product");
        alert("Fail to update");
      });
  }
}

function renderTable(data) {
  let resultHTML = "";
  data.map((item, index) => {
    let itemHTML = `
      <tr class="product-item">
        <td>${index + 1}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>
          <img
          placeholder = anh-san-pham-${item.name}
            class="product-item-img"
            src=${item.img}
          />
        </td>
        <td>${item.type}</td>
        <td class="product-item-desc">
          <div class="product-item-desc-text">
          ${item.desc}
          </div>
        </td>
        <td style="width: 15%">
          <button id="delete-btn-${item.id}" onclick="delelteItem(${
      item.id
    })" class="btn btn-warning"><i class="fa-solid fa-trash-can"></i></button>
          <button id="edit-btn-${item.id}" onclick="editItem(${
      item.id
    })" class="btn btn-primary ml-2"><i class="fa-solid fa-pen-to-square"></i></button>
        </td>
      </tr>`;
    resultHTML += itemHTML;
    document.getElementById("tblDanhSachSP").innerHTML = resultHTML;
  });
}

function getUserInput() {
  let name = document.getElementById("TenSP").value.trim();
  let type = document.getElementById("HangSP").value.trim();
  let price = document.getElementById("GiaSP").value;
  let screen = document.getElementById("ManHinhSP").value.trim();
  let fontCamera = document.getElementById("CameraTruocSP").value.trim();
  let backCamera = document.getElementById("CameraSauSP").value.trim();
  let img = document.getElementById("HinhSP").value.trim();
  let desc = document.getElementById("MoTa").value.trim();
  return new NewPhone(
    name,
    type,
    price,
    screen,
    fontCamera,
    backCamera,
    img,
    desc
  );
}

function hideModal() {
  $("#myModal").modal("hide");
}

function showModal() {
  $("#myModal").modal("show");
}

function showEditInfo(id) {
  let editItem = productList.find((x) => x.id == id);
  document.getElementById("TenSP").value = editItem.name;
  document.getElementById("HangSP").value = editItem.type;
  document.getElementById("GiaSP").value = editItem.price;
  document.getElementById("HinhSP").value = editItem.img;
  document.getElementById("ManHinhSP").value = editItem.screen;
  document.getElementById("CameraTruocSP").value = editItem.fontCamera;
  document.getElementById("CameraSauSP").value = editItem.backCamera;
  document.getElementById("MoTa").value = editItem.desc;
}

function showAddProductModal() {
  document.querySelector(".modal-title").textContent = "Thêm sản phẩm";
  clearInputModal();
  document.getElementById("add-product-btn").style.display = "inline-block";
  document.getElementById("edit-product-btn").style.display = "none";
}

function searchByName() {
  let resultArr = [];
  let searchKey = document
    .getElementById("inputTK")
    .value.toLowerCase()
    .replace(/\s/g, "");
  productList.forEach((item) => {
    let itemName = item.name.toLowerCase().replace(/\s/g, "");
    if (itemName.includes(searchKey)) {
      resultArr.push(item);
    }
  });
  if (resultArr.length != 0) {
    renderTable(resultArr);
  } else {
    alert("Not found !");
  }
}
