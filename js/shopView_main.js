const BASE_URL = "https://62db6ca2e56f6d82a77284cd.mockapi.io/products-data";
const subtotalEl = document.querySelector(".subtotal");
const subtotalPriceEl = document.querySelector(".subtotalPrice");

let dataArr = [];
let cartListArr = [];

function showCartlist() {
  let cartList = document.getElementById("cart-list");
  if (cartList.offsetParent === null) {
    cartList.style.display = "block";
  } else {
    cartList.setAttribute("closing", true);
    cartList.addEventListener(
      "animationend",
      () => {
        cartList.removeAttribute("closing");
        cartList.style.display = "none";
      },
      { once: true }
    );
  }
}

function getProductsData() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      dataArr = res.data;
      turnOffLoading();
      renderShopList(dataArr);
      renderFilterSelection(dataArr);
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
}

getProductsData();
renderCartList(cartListArr);

function renderShopList(data) {
  let productListHtml = "";
  data.forEach((item) => {
    let itemHtml = /*HTML*/ `<div class="col col-4 col-md-4 col-sm-12">
        <div data-id=${item.id} class="product-item">
          <img
          class="item-img"
            src=${item.img}
            alt=""
          />
          <h5>${item.name}</h5>
          <p>
            ${item.desc}
          </p>
          <p>Font camera: ${item.frontCamera}</p>
          <p>Back camera: ${item.backCamera}</p>
          <p>Price: ${item.price} $</p>
          <button onclick= "handleAddToCart(${item.id})"  class="btn btn-dark">Add to cart</button>
        </div>
      </div>`;
    productListHtml += itemHtml;
  });
  document.getElementById("product-list-container").innerHTML = productListHtml;
}

function renderFilterSelection(data) {
  document.getElementById(
    "select-list"
  ).innerHTML = `<option value="All">All</option>`;
  let selectionList = data
    .map((item) => item.type.toLowerCase().capitalize())
    .filter((value, index, arr) => arr.indexOf(value) === index);
  let selectionListHtml = "";
  selectionList.forEach((item) => {
    let itemHtml = `<option value="${item}">${item}</option>`;
    selectionListHtml += itemHtml;
  });
  document.getElementById("select-list").innerHTML += selectionListHtml;
}

function turnOnLoading() {
  document.querySelector(".loading").style.display = "flex";
}

function turnOffLoading() {
  document.querySelector(".loading").style.display = "none";
}

function handleFilter(selection) {
  let selectValue = selection.value.toLowerCase();
  if (selectValue == "all") {
    renderShopList(dataArr);
  } else {
    let filterArr = dataArr.filter(
      (item) => item.type.toLowerCase() === selectValue
    );
    renderShopList(filterArr);
  }
}

Object.defineProperty(String.prototype, "capitalize", {
  value: function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
  },
  enumerable: false,
});

function handleAddToCart(id) {
  if (cartListArr.some((item) => item.id == id)) {
    changeNumberOfUnits("plus", id);
  } else {
    let item = dataArr.find((item) => {
      return item.id == id;
    });
    // console.log(item);
    cartListArr.push({
      ...item,
      numberOfUnits: 1,
    });
    renderCartList(cartListArr);
    renderSubtotal();
  }
}

function renderCartList(data) {
  let cartListHtml = "";
  if (data.length != 0) {
    data.forEach((item) => {
      let itemHtml = /*HTML */ `<li class="cart-item border-bottom">
      <div class="cart-item-img">
        <img
          src=${item.img}
          alt=""
        />
      </div>
      <div class="cart-item-info">
        <h6 class="cart-item-name">${item.name}</h6>
        <div class="quantity-info">
          <span class= "cart-item-price">${item.price}$</span>
          <div class="cart-item-quantity">
            <span >Qty: &nbsp </span>
            <button class="cart-btn" onclick= "changeNumberOfUnits('minus', ${item.id})">-</button>
            <span class="item-quantity">${item.numberOfUnits}</span>
            <button class="cart-btn" onclick="changeNumberOfUnits('plus', ${item.id})">+</button>
          </div>
          <button class="cart-delete-btn btn btn-primary btn-sm" onclick="removeItem(${item.id})">
            <i class="fa-solid fa-trash-can"></i>
          </button>
        </div>
      </div>
    </li>`;
      cartListHtml += itemHtml;
    });
  } else {
    cartListHtml = `<li class="px-3">Chưa có sản phẩm nào trong giỏ hàng</li>`;
  }
  document.getElementById("cart-container").innerHTML = cartListHtml;
}

function updateCart() {
  renderCartList(cartListArr);
  renderSubtotal();
}

//Change quantity
function changeNumberOfUnits(action, id) {
  cartListArr = cartListArr.map((item) => {
    let numberOfUnits = item.numberOfUnits;

    if (item.id == id) {
      if (action === "minus" && numberOfUnits > 1) {
        numberOfUnits -= 1;
      } else if (action === "plus") {
        numberOfUnits += 1;
      }
    }
    return {
      ...item,
      numberOfUnits,
    };
  });

  updateCart();
}

//calculate and render subtotal
function renderSubtotal() {
  let totalPrice = 0;
  let totalItems = 0;

  cartListArr.forEach((item) => {
    totalPrice += item.price * item.numberOfUnits;
    totalItems += item.numberOfUnits;
  });

  subtotalEl.innerHTML = `Quantity: ${totalItems}`;
  subtotalPriceEl.innerHTML = `Price: ${totalPrice.toFixed(2)}$`;
}

//remove item from cart
function removeItem(id) {
  cartListArr = cartListArr.filter((item) => item.id != id);
  updateCart();
}

//purchase button
function purchaseClicked() {
  if (cartListArr.length != 0) {
    alert("Thanks you for your purchase");
    cartListArr = [];
    updateCart();
  }
}
