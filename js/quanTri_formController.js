function onGetDataSpinner() {
  document.getElementById("get-data-spinner").style.display = "inline-block";
}

function offGetDataSpinner() {
  document.getElementById("get-data-spinner").style.display = "none";
}

function onLoadingBtn(btnId) {
  let btn = document.getElementById(btnId);
  btn.disable = true;
  btn.innerHTML = `<span class="spinner-border spinner-border-sm"></span>`;
}

function offLoadingBtn(btnId, btnPrimitiveText) {
  let btn = document.getElementById(btnId);
  btn.innerHTML = btnPrimitiveText;
  btn.disable = false;
}

function clearInputModal() {
  // Clear Input Feild
  let inputfeild = document.querySelectorAll(".modal-body .form-group input");
  inputfeild.forEach((item) => {
    item.value = "";
  });
  document.getElementById("MoTa").value = "";
  // Clear Error Feild
  let errorFeild = document.querySelectorAll(".modal-body .form-group span");
  errorFeild.forEach((item) => {
    item.innerHTML = "";
  });
}

function clearErrorFeild() {
  let errorFeild = document.querySelectorAll(".modal-body .form-group span");
  errorFeild.forEach((item) => {
    item.innerHTML = "";
  });
}

function showModalForEdit() {
  document.getElementById("add-product-btn").style.display = "none";
  document.getElementById("edit-product-btn").style.display = "inline-block";
  document.querySelector(".modal-title").textContent = "Chỉnh sửa sản phẩm";
  clearErrorFeild();
}
